//
//  IndicatorCodeView.swift
//  Login
//
//  Created by Артем Шляхтин on 17.12.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

public enum IndicatorStatus {
    case success
    case failure
    case highlight
    case `default`
}

@IBDesignable
public class IndicatorCodeView: UIImageView {
    
    @IBInspectable public var successColor: UIColor {
        get { return success }
        set (newColor) {
            success = newColor
        }
    }
    
    @IBInspectable public var failureColor: UIColor {
        get { return failure }
        set (newColor) {
            failure = newColor
        }
    }
    
    @IBInspectable public var highlightColor: UIColor {
        get { return highlight }
        set (newColor) {
            highlight = newColor
        }
    }
    
    @IBInspectable public var defaultColor: UIColor {
        get { return colorByDefault }
        set (newColor) {
            colorByDefault = newColor
        }
    }
    
    fileprivate var success = UIColor(red: 167.0/255.0, green: 239.0/255.0, blue: 46.0/255.0, alpha: 1.0)
    fileprivate var failure = UIColor(red: 250.0/255.0, green: 50.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    fileprivate var highlight = UIColor(red: 246.0/255.0, green: 246.0/255.0, blue: 246.0/255.0, alpha: 1.0)
    fileprivate var colorByDefault = UIColor(red: 76.0/255.0, green: 76.0/255.0, blue: 76.0/255.0, alpha: 1.0)
    
    // MARK: - Initialization

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        performInitialization()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        performInitialization()
    }
    
    deinit {
        removeNotification()
    }
    
    
    // MARK: - Actions
    
    public func changeStatus(_ status: IndicatorStatus, animated: Bool) {
        var toColor: CGColor?
        var toOpacity: Float = 0.0
        
        switch (status) {
            case .success:
                toColor = self.success.cgColor
                toOpacity = 1.0
            
            case .failure:
                toColor = self.failure.cgColor
                toOpacity = 1.0
            
            case .highlight:
                toColor = self.highlight.cgColor
                toOpacity = 1.0
            
            case .default:
                toColor = self.colorByDefault.cgColor
                toOpacity = 0.0
        }
        
        if animated {
            let speed: TimeInterval = 0.2
            self.addBasicAnimation("backgroundColor", fromValue: self.layer.backgroundColor, toValue: toColor, duration: speed, timingFunction: kCAMediaTimingFunctionLinear, repeatCount: 0)
        	self.addBasicAnimation("shadowOpacity", fromValue: self.layer.shadowOpacity as AnyObject?, toValue: toOpacity as AnyObject?, duration: speed, timingFunction: kCAMediaTimingFunctionLinear, repeatCount: 0)
        }
        self.layer.shadowColor = (toOpacity == 1.0) ? toColor : self.layer.backgroundColor
        self.layer.backgroundColor = toColor
        self.layer.shadowOpacity = toOpacity
    }
    
    public func blinkFailure() {
        let speed: TimeInterval = 0.2
        let toOpacity: Float = 1.0
        let toColor = self.failure.cgColor
        self.addBasicAnimation("backgroundColor", fromValue: self.layer.backgroundColor, toValue: toColor, duration: speed, timingFunction: kCAMediaTimingFunctionEaseOut, repeatCount: 3)
        self.addBasicAnimation("shadowOpacity", fromValue: self.layer.shadowOpacity as AnyObject?, toValue: toOpacity as AnyObject?, duration: speed, timingFunction: kCAMediaTimingFunctionEaseOut, repeatCount: 3)
        self.layer.shadowColor = toColor
        self.layer.backgroundColor = toColor
        self.layer.shadowOpacity = toOpacity
    }
    
    
    // MARK: - Properties
    
    @IBInspectable open var enableShadow: Bool {
        get { return (self.layer.shadowOpacity == 1.0) ? true : false }
        set (value) { self.layer.shadowOpacity = (value) ? 1.0 : 0.0 }
    }
    
    
    // MARK: - Notification
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "layer.shadowOpacity" {
            guard let newValue = change?[NSKeyValueChangeKey.newKey] as? CGFloat else { return }
            if newValue == 1.0 || newValue == 0.0 { return }
            self.enableShadow = false
            print("observer")
        }
    }
    
    
    // MARK: - Private
    
    fileprivate func performInitialization() {
        configureShadow()
        addNotification()
    }
    
    fileprivate func configureShadow() {
        self.layer.shadowColor = self.backgroundColor?.cgColor
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    }
    
    fileprivate func addNotification() {
        self.addObserver(self, forKeyPath: "layer.shadowOpacity", options: .new, context: &self.layer.shadowOpacity)
    }
    
    fileprivate func removeNotification() {
        self.removeObserver(self, forKeyPath: "layer.shadowOpacity")
    }
    
    fileprivate func addBasicAnimation(_ key: String, fromValue: AnyObject?, toValue: AnyObject?, duration: TimeInterval, timingFunction: String, repeatCount: Float) {
        let animation = CABasicAnimation(keyPath: key)
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = duration
        animation.timingFunction = CAMediaTimingFunction(name: timingFunction)
        animation.repeatCount = repeatCount
        animation.isRemovedOnCompletion = true
        self.layer.add(animation, forKey: key)
    }
}
