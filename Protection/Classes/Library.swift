//
//  Library.swift
//  Protection
//
//  Created by Артём Шляхтин on 11/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

public struct ProtectionFramework {
    public static let bundleName = "ru.roseurobank.Protection"
}

// MARK: - Enum

public enum AuthenticationStage {
    case phone
    case confirm
    case sendAgainSMS
    case password
    case pin
    case touch
    case success
    case error
}

// MARK: - Structs

public struct ButtonColor {
    public var text: StateColor
    public var background: StateColor
    
    public init() {
        text = StateColor()
        background = StateColor()
    }
}

public struct StateColor {
    public var active: UIColor
    public var inactive: UIColor

    public init() {
        active = UIColor.blue
        inactive = UIColor.lightGray
    }
}

// MARK: - Protocols

public protocol ProtectionProtocol: NSObjectProtocol {
    var backgroundColor: UIColor { get }
}

public protocol ProtectionCustomizeDelegate: ProtectionProtocol {
    var buttonColor: ButtonColor { get }
    var textColor: UIColor { get }
}

public protocol AuthenticationDelegate: NSObjectProtocol {
    func authentication(_ controller: UIViewController, phoneNumber phone: String, completion: ((_ responce: ResponseAuthentication) -> Void)?)
    func authentication(_ controller: UIViewController, confirmSMS code: String, completion: ((_ responce: ResponseAuthentication) -> Void)?)
    func authentication(_ controller: UIViewController, password: String, completion: ((_ responce: ResponseAuthentication) -> Void)?)
    func authentication(_ controller: UIViewController, newCode code: String, completion: ((_ responce: ResponseAuthentication) -> Void)?)
    func enableTouchID(_ controller: UIViewController, state: Bool, code: String, completion: ((_ responce: ResponseAuthentication) -> Void)?)
    func sendCodeAgain(in controller: UIViewController, completion: ((_ responce: ResponseAuthentication) -> Void)?)
    func recoveryPassword(in controller: UIViewController, completion: ((_ responce: ResponseAuthentication) -> Void)?)
}

public extension AuthenticationDelegate {
    func authentication(_ controller: UIViewController, password: String, completion: ((_ responce: ResponseAuthentication) -> Void)?) {}
    func recoveryPassword(in controller: UIViewController, completion: ((_ responce: ResponseAuthentication) -> Void)?) {}
}

public protocol LockDelegate: NSObjectProtocol {
    var isEnableTouchID: Bool { get }
    func protection(_ controller: UIViewController, verifyByCode code: String, completion: ((_ responce: ResponseProtection) -> Void)?)
    func protection(_ controller: UIViewController, verifyByBiometrics success: Bool, completion: ((_ responce: ResponseProtection) -> Void)?)
    func protectionLogout(_ controller: UIViewController)
}

public typealias ResponseAuthentication = (stage: AuthenticationStage, data: Any?)
public typealias ResponseProtection = (success: Bool, error: Error?)
