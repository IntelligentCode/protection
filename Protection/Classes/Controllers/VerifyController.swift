//
//  VerifyController.swift
//  Protection
//
//  Created by Артём Шляхтин on 13/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import FormatField

// MARK: - Lifecycle

public class VerifyController: ProtectionTableViewController {
    
    @IBOutlet weak var codeField: CardCodeField!
    @IBOutlet weak var buttonSendAgain: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var status: UILabel!
    
    weak var protectionDelegate: AuthenticationDelegate?
    var phone: String = ""
    
    fileprivate let secondsToSendCodeAgain = 120
    fileprivate var timer = Timer()
    fileprivate var counter = 0
    fileprivate var isFailure = false

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureButton()
        configureColor()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        authentication()
        createTimer()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeTimer()
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Outlet Actions
    
    @IBAction func sendAgain(_ sender: Any) {
        createTimer()
        status.text = ""
        codeField.text = ""
        
        if isFailure {
            authentication()
        } else {
            protectionDelegate?.sendCodeAgain(in: self, completion: nil)
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - Navigation

extension VerifyController {
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "New Code Segue" {
            let codeController = segue.destination as? CodeController
            codeController?.customizeDelegate = customizeDelegate
            codeController?.protectionDelegate = protectionDelegate
        }
        
        if segue.identifier == "Enter Code Segue" {
            let passwordController = segue.destination as? PasswordController
            passwordController?.customizeDelegate = customizeDelegate
            passwordController?.protectionDelegate = protectionDelegate
            passwordController?.isShowCloseButton = isShowCloseButton
        }
    }
    
}

// MARK: - Actions

extension VerifyController {
    
    func authentication() {
        isFailure = false
        protectionDelegate?.authentication(self, phoneNumber: phone, completion: responseConfirmRequest)
    }
    
    func confirm() {
        isFailure = false
        status.text = ""
        activity.startAnimating()
        buttonSendAgain.isHidden = true
        counter = secondsToSendCodeAgain
        
        codeField.resignFirstResponder()
        guard let code = codeField.text else { return }
        protectionDelegate?.authentication(self, confirmSMS: code, completion: responseConfirmRequest)
    }
    
    func failure(_ error: Error) {
        activity.stopAnimating()
        isFailure = true
        self.status.text = error.localizedDescription
        counter = secondsToSendCodeAgain
        buttonSendAgain.isHidden = false
    }
    
}

// MARK: - Table View Delegate

extension VerifyController {
    
}

// MARK: - Table View Data Source

extension VerifyController {
    
    override func updateTableInsets(height: CGFloat) {
        let rowHeight: CGFloat = tableView.rowHeight
        let numberOfRow: CGFloat = 2
        let offset = (height-rowHeight*numberOfRow)/2.0
        tableView.contentInset = UIEdgeInsetsMake(offset, 0, -offset, 0)
    }
    
}

// MARK: - Text Field Delegate

extension VerifyController: UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let numberPadDone = PadAccessory(textField: textField, delegate: self)
        textField.inputAccessoryView = numberPadDone
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        guard let templateTextField = textField as? FormatTextField else { return true }
        return templateTextField.verify()
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let templateTextField = textField as? FormatTextField else { return true }
        let result = templateTextField.formatTextField(range, replacementText: string)
        if templateTextField.isFormatFull {
            if templateTextField.resignFirstResponder() {
                confirm()
            }
        }
        return result
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

// MARK: - Number Pad Accessory Delegate

extension VerifyController: PadAccessoryDelegate {
    
    public func padToolbarDidClickLeftButton(_ textField: UITextField) {
        if let templateField = textField as? FormatTextField {
            templateField.cancel()
        }
        textField.resignFirstResponder()
    }
    
    public func padToolbarDidClickRightButton(_ textField: UITextField) {
        textField.resignFirstResponder()
        confirm()
    }
    
    public func padToolbarLeftButtonName(_ textField: UITextField) -> String? {
        return "Отмена"
    }
    
    public func padToolbarRightButtonName(_ textField: UITextField) -> String? {
        return "Готово"
    }
    
}

// MARK: - Keyboard

extension VerifyController {
    
    override var scrollToFrame: CGRect {
        get { return codeField.frame }
    }
    
}

// MARK: - Timer

extension VerifyController {
    
    func createTimer() {
        counter = 0
        buttonSendAgain.isEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(VerifyController.updateCounter), userInfo: nil, repeats: true)
    }
    
    func removeTimer() {
        if timer.isValid { timer.invalidate() }
    }
    
    func updateCounter() {
        counter += 1
        if counter >= secondsToSendCodeAgain {
            timer.invalidate()
            buttonSendAgain.setTitle("Выслать код повторно", for: .normal)
            buttonSendAgain.setTitle("Выслать код повторно через \(secondsToSendCodeAgain) сек", for: .disabled)
            buttonSendAgain.isEnabled = true
            return
        }
        
        UIView.performWithoutAnimation {
            buttonSendAgain.setTitle("Выслать код повторно через \(secondsToSendCodeAgain-counter) сек", for: .disabled)
            buttonSendAgain.layoutIfNeeded()
        }
    }
    
}

// MARK: - Callback

extension VerifyController {
    
    func responseConfirmRequest(responce: ResponseAuthentication) {
        switch responce.stage {
            case .password: performSegue(withIdentifier: "Enter Code Segue", sender: nil)
            case .pin: performSegue(withIdentifier: "New Code Segue", sender: nil)
            case .error: prepareError(exception: responce.data)
            default: break
        }
    }
    
    fileprivate func prepareError(exception: Any?) {
        if let error = exception as? Error {
            failure(error)
        }
    }
    
}

// MARK: - Configuration

extension VerifyController {
    
    func configureButton() {
        guard let active = customizeDelegate?.buttonColor.background.active,
              let inactive = customizeDelegate?.textColor
        else { return }
        
        buttonSendAgain.setTitleColor(active, for: .normal)
        buttonSendAgain.setTitleColor(inactive, for: .disabled)
    }
    
    func configureColor() {
        guard let color = customizeDelegate?.textColor else { return }
        status.textColor = color
        activity.color = color
    }
    
}
