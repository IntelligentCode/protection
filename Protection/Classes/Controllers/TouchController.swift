//
//  TouchController.swift
//  Protection
//
//  Created by Артём Шляхтин on 15/06/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

public class TouchController: UIViewController {
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    public weak var customizeDelegate: ProtectionCustomizeDelegate?
    public weak var protectionDelegate: AuthenticationDelegate?
    public var onCompletion: ((_ responce: ResponseAuthentication) -> Void)?
    var code: String = ""

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureText()
        configureButtons()
        self.navigationItem.hidesBackButton = true
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func accept(_ sender: Any) {
        protectionDelegate?.enableTouchID(self, state: true, code: code, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        protectionDelegate?.enableTouchID(self, state: false, code: code, completion: nil)
    }
    
}

// MARK: - Actions

extension TouchController {

    
    
}

// MARK: - Configure

extension TouchController {
    
    func configureText() {
        guard let color = customizeDelegate?.textColor else { return }
        descLabel.textColor = color
    }
    
    func configureButtons() {
        guard let buttonColor = customizeDelegate?.buttonColor else { return }
        
        let textColor = buttonColor.text
        acceptButton.setTitleColor(textColor.active, for: .normal)
        acceptButton.setTitleColor(textColor.inactive, for: .disabled)
        
        let backgroundColor = buttonColor.background
        let backgroundActive = UIImage.imageWithColor(color: backgroundColor.active)
        let backgroundInactive = UIImage.imageWithColor(color: backgroundColor.inactive)
        acceptButton.setBackgroundImage(backgroundActive, for: .normal)
        acceptButton.setBackgroundImage(backgroundInactive, for: .disabled)
        
        cancelButton.setTitleColor(backgroundColor.active, for: .normal)
    }
}
