//
//  AuthenticationController.swift
//  Protection
//
//  Created by Артём Шляхтин on 11/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import System
import FormatField

// MARK: - Lifecycle

public class AuthenticationController: ProtectionTableViewController {
    
    @IBOutlet weak var phoneField: PhoneField!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    
    public weak var protectionDelegate: AuthenticationDelegate?
    fileprivate var isContinue: Bool = false
    fileprivate var cancel: Bool = false
    fileprivate var phone: String = ""

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureText()
        configureButton()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Outlets
    
    @IBAction func login(_ sender: Any) {
        verify()
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Actions 

extension AuthenticationController {
    
    func verify() {
        self.view.endEditing(true)
        if isContinue && isKeyboardShow == false {
            performSegue(withIdentifier: "Verify Segue", sender: nil)
        }
    }
    
}

// MARK: - Navigation

extension AuthenticationController {
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Verify Segue" {
            let verifyController = segue.destination as? VerifyController
            verifyController?.customizeDelegate = customizeDelegate
            verifyController?.protectionDelegate = protectionDelegate
            verifyController?.isShowCloseButton = isShowCloseButton
            verifyController?.phone = phone
        }
    }
    
}

// MARK: - Table View Delegate

extension AuthenticationController {
    
}

// MARK: - Table View Data Source

extension AuthenticationController {
    
}

// MARK: - Configuration

extension AuthenticationController {
    
    func configureText() {
        guard let color = customizeDelegate?.textColor else { return }
        descLabel.textColor = color
    }
    
    func configureButton() {
        buttonDone.isEnabled = false
        configureButtonColor()
    }
    
    private func configureButtonColor() {
        guard let buttonColor = customizeDelegate?.buttonColor else { return }
        
        let textColor = buttonColor.text
        buttonDone.setTitleColor(textColor.active, for: .normal)
        buttonDone.setTitleColor(textColor.inactive, for: .disabled)
        
        let backgroundColor = buttonColor.background
        let backgroundActive = UIImage.imageWithColor(color: backgroundColor.active)
        let backgroundInactive = UIImage.imageWithColor(color: backgroundColor.inactive)
        buttonDone.setBackgroundImage(backgroundActive, for: .normal)
        buttonDone.setBackgroundImage(backgroundInactive, for: .disabled)
    }
    
}

// MARK: - Text Field Delegate

extension AuthenticationController: UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        cancel = false
        let numberPadDone = PadAccessory(textField: textField, delegate: self)
        textField.inputAccessoryView = numberPadDone
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        guard let templateTextField = textField as? FormatTextField,
              let length = templateTextField.text?.characters.count
        else { return true }
        
        let isVerify = templateTextField.verify()
        isContinue = (isVerify && length > 0) ? true : false
        return isVerify
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let templateTextField = textField as? FormatTextField else { return true }
        let result = templateTextField.formatTextField(range, replacementText: string)
        buttonDone.isEnabled = templateTextField.isFormatFull
        return result
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        buttonDone.isEnabled = false
        isContinue = false
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, text.isEmpty == false else { return }
        phone = text.clearPhoneFormat()
    }
    
}

// MARK: - Number Pad Accessory Delegate

extension AuthenticationController: PadAccessoryDelegate {
    
    public func padToolbarDidClickLeftButton(_ textField: UITextField) {
        if let templateField = textField as? FormatTextField {
            cancel = true
            templateField.cancel()
            if let state = textField.text?.isEmpty {
                buttonDone.isEnabled = !state
            }
        }
        textField.resignFirstResponder()
    }
    
    public func padToolbarDidClickRightButton(_ textField: UITextField) {
        textField.resignFirstResponder()
        verify()
    }
    
    public func padToolbarLeftButtonName(_ textField: UITextField) -> String? {
        return "Отмена"
    }
    
    public func padToolbarRightButtonName(_ textField: UITextField) -> String? {
        return "Готово"
    }
    
}

// MARK: - Keyboard

extension AuthenticationController {
    
    override var scrollToFrame: CGRect {
        get { return buttonDone.frame }
    }
    
    override func keyboardDidHide(notification: Notification) {
        if isKeyboardShow == false { return }
        
        super.keyboardDidHide(notification: notification)
        if isContinue && cancel == false {
            performSegue(withIdentifier: "Verify Segue", sender: nil)
        }
    }
    
}
