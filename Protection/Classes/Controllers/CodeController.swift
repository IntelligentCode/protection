//
//  CodeController.swift
//  Protection
//
//  Created by Артём Шляхтин on 13/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import AudioToolbox
import LocalAuthentication

// MARK: - Lifecycle

public class CodeController: UIViewController {
    
    @IBOutlet weak var enterCodeLabel: UILabel!
    @IBOutlet weak var repeateCodeLabel: UILabel!
    
    @IBOutlet var indicators: [IndicatorCodeView]!
    @IBOutlet var numbers: [UIButton]!
    @IBOutlet weak var repeateCodeStackView: UIStackView!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    
    public weak var customizeDelegate: ProtectionCustomizeDelegate?
    public weak var protectionDelegate: AuthenticationDelegate?
    public var isShowCloseButton = false
    
    fileprivate var digit = [Int](repeating: -1, count: 8)
    fileprivate var isFailure = false
    fileprivate let DigitError = -1
    fileprivate var code: String = ""

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationController()
        configureText()
        configureButtons()
        
        self.view.backgroundColor = customizeDelegate?.backgroundColor
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isShowCloseButton == false { navigationItem.rightBarButtonItem = nil }
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Outlets
    
    @IBAction func change(_ sender: AnyObject) {
        if isFailure { resetIndicators() }
        
        let index = currentIndex
        if index == DigitError { return }
        if index == halfDigit { showRepeatCode(true) }
        
        self.digit[index] = sender.tag
        changeStatusIndicator(.highlight, at: index)
        if index == digit.count-1 { checkCode() }
    }
    
    @IBAction func remove(_ sender: AnyObject) {
        if isFailure {
            resetIndicators()
            return
        }
        
        let index = previousIndex
        if index == DigitError { return }
        if index == halfDigit { showRepeatCode(false) }
        
        self.digit[index] = -1
        changeStatusIndicator(.default, at: index)
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Navigation

extension CodeController {
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TouchID Segue" {
            let controller = segue.destination as? TouchController
            controller?.customizeDelegate = customizeDelegate
            controller?.protectionDelegate = protectionDelegate
            controller?.code = code
        }
    }
    
}

// MARK: - Calculate Properties

extension CodeController {
    
    var currentIndex: Int {
        get {
            guard let cursor = digit.index(of: -1) else { return DigitError }
            return cursor
        }
    }
    
    var previousIndex: Int {
        get {
            let index = currentIndex-1
            if index <= DigitError { return DigitError }
            return index
        }
    }
    
    var halfDigit: Int {
        get {
            let count = indicators.count-1
            let halfCount = (count / 2)
            return halfCount
        }
    }
    
}

// MARK: - Actions

extension CodeController {
    
    fileprivate func moveTouchIdScreenIfNeeded() {
        let authenticationContext = LAContext()
        var error:NSError?
        
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            protectionDelegate?.enableTouchID(self, state: false, code: "", completion: nil)
            return
        }
        
        performSegue(withIdentifier: "TouchID Segue", sender: nil)
    }
    
    fileprivate func changeStatusIndicator(_ status: IndicatorStatus, at index: Int) {
        let indicator = self.indicators[index]
        indicator.changeStatus(status, animated: true)
    }
    
    fileprivate func showRepeatCode(_ state: Bool) {
        let duration = UIApplication.shared.statusBarOrientationAnimationDuration
        UIView.animate(withDuration: duration, animations: { [weak weakSelf = self] () -> Void in
            weakSelf?.repeateCodeStackView.layer.opacity = (state) ? 1.0 : 0.0
        })
    }
    
    fileprivate func checkCode() {
        let code = "\(digit[0])\(digit[1])\(digit[2])\(digit[3])"
        let secondCode = "\(digit[4])\(digit[5])\(digit[6])\(digit[7])"
        
        switch (code == secondCode) {
             case true: configureBeforeRequest(with: "Привязываю устройство...")
                        protectionDelegate?.authentication(self, newCode: code, completion: responseBindRequest)
                        self.code = code
            
            case false: blinkFailure()
        }
    }
    
    fileprivate func resetIndicators() {
        indicators.forEach{ $0.changeStatus(.default, animated: true)}
        digit = [Int](repeating: -1, count: 8)
        isFailure = false
        showRepeatCode(false)
    }
    
    fileprivate func blinkFailure() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        indicators.forEach{ $0.blinkFailure() }
        isFailure = true
    }
    
}

// MARK: - Configure Code Controller 

extension CodeController {
    
    func configureText() {
        guard let color = customizeDelegate?.textColor else { return }
        enterCodeLabel.textColor = color
        repeateCodeLabel.textColor = color
    }
    
    func configureButtons() {
        guard let active = customizeDelegate?.buttonColor.background.active else { return }
        
        numbers.forEach { (number) in
            number.setTitleColor(active, for: .normal)
            number.tintColor = active
        }
        
        indicators.forEach { (indicator) in
            indicator.highlightColor = active
        }
    }
    
    func configureBeforeRequest(with text: String) {
        let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activity.color = customizeDelegate?.textColor
        activity.startAnimating()
        
        let label = UILabel(frame: CGRect.zero)
        label.text = text
        if let attributes = UINavigationBar.appearance().titleTextAttributes {
            if let font = attributes[NSFontAttributeName] as? UIFont {
                label.font = font
            }
            
            if let color = attributes[NSForegroundColorAttributeName] as? UIColor {
                label.textColor = color
            }
        }
        
        
        
        let stack = UIStackView(frame: CGRect.zero)
        stack.axis = .horizontal
        stack.distribution = .fillProportionally
        stack.alignment = .fill
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.addArrangedSubview(activity)
        stack.addArrangedSubview(label)
        
        self.navigationItem.titleView = stack
    }
    
    func configureAfterRequest() {
        if navigationItem.titleView != nil { navigationItem.titleView = nil }
    }
    
    func showAlertWithTitle(_ title:String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(ok)
        OperationQueue.main.addOperation {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func configureNavigationController() {
        let clearImage = UIImage()
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(clearImage, for: .default)
        navigationBar?.shadowImage = clearImage
        navigationBar?.isTranslucent = true
        
        self.navigationItem.hidesBackButton = true
    }
}

// MARK: - Callback

extension CodeController {
    
    func responseBindRequest(responce: ResponseAuthentication) {
        configureAfterRequest()
        
        switch responce.stage {
            case .success: print("success")
            case .touch: moveTouchIdScreenIfNeeded()
            case .error: prepareError(exception: responce.data)
            default: break
        }
    }
    
    fileprivate func prepareError(exception: Any?) {
        if let error = exception as? Error {
            isFailure = true
            showAlertWithTitle("Ошибка", message: error.localizedDescription)
        }
    }
    
}
