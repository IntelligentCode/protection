//
//  TableViewController.swift
//  Protection
//
//  Created by Артём Шляхтин on 13/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

// MARK: - Lifecycle

public class ProtectionTableViewController: UITableViewController {
    
    public weak var customizeDelegate: ProtectionCustomizeDelegate?
    public var isShowCloseButton: Bool = false
    var isKeyboardShow: Bool = false

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureTable()
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotification()
        configureCloseButton()
    }
    
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if isKeyboardShow == false {
            updateTableInsets(height: tableView.frame.height)
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotification()
    }
    
}

// MARK: - Table View Delegate

extension ProtectionTableViewController {
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = customizeDelegate?.backgroundColor
    }
    
    func updateTableInsets(height: CGFloat) {
        let rowHeight: CGFloat = tableView.rowHeight
        let numberOfRow: CGFloat = CGFloat(tableView.numberOfRows(inSection: 0))
        let offset = (height-rowHeight*numberOfRow)/2.0
        tableView.contentInset = UIEdgeInsetsMake(offset, 0, -offset, 0)
    }
    
}

// MARK: - Configuration

extension ProtectionTableViewController {
    
    func configureNavigationBar() {
        let clearImage = UIImage()
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(clearImage, for: .default)
        navigationBar?.shadowImage = clearImage
        navigationBar?.isTranslucent = true
    }
    
    func configureTable() {
        self.tableView.backgroundColor = customizeDelegate?.backgroundColor
    }
    
    func configureCloseButton() {
        if isShowCloseButton == false {
            navigationItem.rightBarButtonItem = nil
        }
    }
}

// MARK: - Notification

extension ProtectionTableViewController {
    
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(ProtectionTableViewController.keyboardWillShown(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProtectionTableViewController.keyboardWillBeHidden(notification:)), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProtectionTableViewController.keyboardDidHide(notification:)), name: .UIKeyboardDidHide, object: nil)
    }
    
    func unregisterNotification() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardDidHide, object: nil)
    }
    
}

// MARK: - Keyboard

extension ProtectionTableViewController {
    
    var scrollToFrame: CGRect {
        get { return CGRect.zero }
    }
    
    func keyboardWillShown(notification: Notification) {
        isKeyboardShow = true
        
        guard let scrollView = tableView,
            let info = notification.userInfo,
            let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        updateTableInsets(height: tableView.frame.height-keyboardSize.height)
        var frame = self.view.frame
        frame.size.height -= keyboardSize.height
        if frame.contains(scrollToFrame.origin) {
            scrollView.scrollRectToVisible(scrollToFrame, animated: true)
        }
    }
    
    func keyboardWillBeHidden(notification: Notification) {
        updateTableInsets(height: tableView.frame.height)
    }
    
    func keyboardDidHide(notification: Notification) {
        isKeyboardShow = false
    }
    
}

