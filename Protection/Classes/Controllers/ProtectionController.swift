//
//  ProtectionController.swift
//  Protection
//
//  Created by Артём Шляхтин on 22/06/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import AudioToolbox
import LocalAuthentication

public class ProtectionController: UIViewController {
    
    @IBOutlet var indicators: [IndicatorCodeView]!
    @IBOutlet var numbers: [UIButton]!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var topViewStack: NSLayoutConstraint!
    @IBOutlet weak var bottomViewStack: NSLayoutConstraint!
    
    public weak var customizeDelegate: ProtectionCustomizeDelegate?
    public weak var lockDelegate: LockDelegate?
    
    fileprivate var digit = [Int](repeating: -1, count: 4)
    fileprivate let digitError = -1
    fileprivate var isFailure = false
    fileprivate let indexTouchButton = 10
    
    fileprivate var topOffset: CGFloat = 0
    fileprivate var bottomOffset: CGFloat = 0
    
    // MARK: - Lifecycle
    
    public override func loadView() {
        super.loadView()
        topOffset = topViewStack.constant
        bottomOffset = bottomViewStack.constant
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureNavigation()
        configureStatus()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetIndicators()
    }
    
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        topViewStack.constant = topOffset+topLayoutGuide.length
        bottomViewStack.constant = bottomOffset+bottomLayoutGuide.length
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let isEnableTouchID = lockDelegate?.isEnableTouchID else { return }
        if isEnableTouchID { authenticationWithBiometrics() }
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
 
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // MARK: - Actions
    
    @IBAction func change(_ sender: AnyObject) {
        if isFailure {
            resetIndicators()
            status.text = nil
        }
        
        let index = currentIndex
        if index == digitError { return }
        
        digit[index] = sender.tag
        changeStatusIndicator(.highlight, at: index)
        if index == digit.count-1 { verifyCode() }
    }
    
    @IBAction func remove(_ sender: AnyObject) {
        if isFailure {
            resetIndicators()
            status.text = nil
            return
        }

        let index = previousIndex
        if index == digitError { return }
        
        self.digit[index] = -1
        changeStatusIndicator(.default, at: index)
    }
    
    @IBAction func verifyByTouchID(_ sender: AnyObject) {
        authenticationWithBiometrics()
    }

    func logout() {
        lockDelegate?.protectionLogout(self)
    }
    
    fileprivate func touchVerify(_ success: Bool, error: Error?) {
        OperationQueue.main.addOperation { [unowned self] in
            if success { self.configureBeforeRequest() }
            self.lockDelegate?.protection(self, verifyByBiometrics: success, completion: self.responseRequest(responce:))
        }
    }
    
    fileprivate func verifyCode() {
        let code = "\(digit[0])\(digit[1])\(digit[2])\(digit[3])"
        configureBeforeRequest()
        lockDelegate?.protection(self, verifyByCode: code, completion: responseRequest(responce:))
    }
    
    fileprivate func changeStatusIndicator(_ status: IndicatorStatus, at index: Int) {
        let indicator = self.indicators[index]
        indicator.changeStatus(status, animated: true)
    }
    
    fileprivate func resetIndicators() {
        indicators.forEach{ $0.changeStatus(.default, animated: true)}
        digit = [Int](repeating: -1, count: 4)
        isFailure = false
    }
    
    fileprivate func blinkSuccess() {
        indicators.forEach{ $0.changeStatus(.success, animated: true) }
        isFailure = false
    }
    
    fileprivate func blinkFailure() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        indicators.forEach{ $0.blinkFailure() }
        isFailure = true
    }
    
}

// MARK: - Calculate Properties

extension ProtectionController {
    
    var currentIndex: Int {
        get {
            guard let cursor = digit.index(of: -1) else { return digitError }
            return cursor
        }
    }
    
    var previousIndex: Int {
        get {
            var index = currentIndex-1
            if index == -2 { index = digit.count-1 }
            if index <= digitError { return digitError }
            return index
        }
    }
    
}

// MARK: - Touch ID

extension ProtectionController {
    
    func authenticationWithBiometrics() {
        let authenticationContext = LAContext()
        var error:NSError?
        
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            numbers[indexTouchButton].isEnabled = false
            return
        }
        
        let text = "Приложите палец, чтобы войти в приложение"
        authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: text, reply: touchVerify(_:error:))
    }
    
    fileprivate func errorMessageForLAErrorCode(_ errorCode: Int) -> String{
        var message = ""
        
        switch errorCode {
        case LAError.Code.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.Code.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.Code.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.Code.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.Code.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.Code.touchIDLockout.rawValue:
            message = "Too many failed attempts."
            
        case LAError.Code.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
            
        case LAError.Code.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.Code.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = "Did not find error code on LAError object"
        }
        
        return message
    }
    
}

// MARK: - Configuration

extension ProtectionController {
    
    fileprivate func configureBeforeRequest() {
        status.text = nil
        activity.startAnimating()
    }
    
    fileprivate func configureAfteRequest() {
        activity.stopAnimating()
    }
    
    fileprivate func configureStatus() {
        guard let color = customizeDelegate?.textColor else { return }
        status.textColor = color
        status.text = nil
    }
    
    fileprivate func configureView() {
        self.view.backgroundColor = customizeDelegate?.backgroundColor
        configureButtons()
        configureTouchButton()
    }
    
    fileprivate func configureNavigation() {
        configureNavigationBar()
        configureNavigationButton()
        self.navigationItem.hidesBackButton = true
    }
    
    fileprivate func configureTouchButton() {
        var isEnableTouchID = false
        if let state = lockDelegate?.isEnableTouchID {
            isEnableTouchID = state
        }
        
        let currentBundle = Bundle(identifier: ProtectionFramework.bundleName)
        let inactive = UIImage(named: "Button Touch ID Inactive", in: currentBundle, compatibleWith: nil)
        let active = UIImage(named: "Button Touch ID Active", in: currentBundle, compatibleWith: nil)
    
        let touchButton = numbers[indexTouchButton]
        touchButton.setImage(active, for: .normal)
        touchButton.setImage(inactive, for: .disabled)
        touchButton.isEnabled = isEnableTouchID
    }
    
    fileprivate func configureButtons() {
        guard let active = customizeDelegate?.buttonColor.background.active else { return }
        
        numbers.forEach { (number) in
            number.setTitleColor(active, for: .normal)
            number.tintColor = active
        }
        
        indicators.forEach { (indicator) in
            indicator.highlightColor = active
        }
    }
    
    //TODO: Вынесити в делегат настройку иконки
    fileprivate func configureNavigationButton() {
        let frameworkBundle = Bundle(identifier: ProtectionFramework.bundleName)
        let logoutImage = UIImage(named: "Logout", in: frameworkBundle, compatibleWith: nil)
        let logoutButton = UIBarButtonItem(image: logoutImage, style: .plain, target: self, action: #selector(ProtectionController.logout))
        self.navigationItem.rightBarButtonItem = logoutButton
    }
    
    fileprivate func configureNavigationBar() {
        let clearImage = UIImage()
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(clearImage, for: .default)
        navigationBar?.shadowImage = clearImage
        navigationBar?.isTranslucent = true
    }
    
}

// MARK: - Callback

extension ProtectionController {
    
    func responseRequest(responce: ResponseProtection) {
        configureAfteRequest()
        switch responce.success {
            case true: success()
           case false: prepareError(exception: responce.error)
        }
    }
    
    fileprivate func success() {
        blinkSuccess()
    }
    
    fileprivate func prepareError(exception: Error?) {
        blinkFailure()
        status.text = exception?.localizedDescription
    }
    
}
