//
//  PasswordController.swift
//  Protection
//
//  Created by Артём Шляхтин on 29/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import System

// MARK: - Lifecycle

public class PasswordController: ProtectionTableViewController {
    
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var recoveryButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    public weak var protectionDelegate: AuthenticationDelegate?
    fileprivate let passwordLength = 7
    fileprivate var isContinue: Bool = false

    override public func viewDidLoad() {
        super.viewDidLoad()
        configureColor()
        configureButton()
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Outlet Actions

    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirm(_ sender: Any) {
        verify()
    }
    
    @IBAction func recovery(_ sender: Any) {
        self.protectionDelegate?.recoveryPassword(in: self, completion: nil)
    }
    
    @IBAction func textFieldChanged(_ textField: UITextField) {
        guard let characters = textField.text?.characters.count else { return }
        doneButton.isEnabled = (characters > passwordLength) ? true : false
    }
}

// MARK: - Navigation

extension PasswordController {
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "New Code Segue" {
            let codeController = segue.destination as? CodeController
            codeController?.customizeDelegate = customizeDelegate
            codeController?.protectionDelegate = protectionDelegate
        }
    }
    
}

// MARK: - Actions

extension PasswordController {
    
    func verify() {
        self.view.endEditing(true)
        if let passw = passwordField.text {
            activity.startAnimating()
            status.text = ""
            recoveryButton.isHidden = true
            doneButton.isEnabled = false
            protectionDelegate?.authentication(self, password: passw, completion: responseCompletedRequest)
        }
    }
    
    func failure(_ error: Error) {
        self.status.text = error.localizedDescription
    }
    
}

// MARK: - Table View Data Source

extension PasswordController {
    
    override func updateTableInsets(height: CGFloat) {
        let rowHeight: CGFloat = tableView.rowHeight
        let numberOfRow: CGFloat = 2
        let offset = (height-rowHeight*numberOfRow)/2.0
        tableView.contentInset = UIEdgeInsetsMake(offset, 0, -offset, 0)
    }
    
}

// MARK: - Text Field Delegate

extension PasswordController: UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        isContinue = false
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let count = textField.text?.characters.count, count > passwordLength {
            verify()
        }
        textField.resignFirstResponder()
        return true
    }
    
}

// MARK: - Configuration

extension PasswordController {
    
    func configureColor() {
        guard let color = customizeDelegate?.textColor else { return }
        status.textColor = color
        activity.color = color
    }
    
    func configureButton() {
        doneButton.isEnabled = false
        configureButtonColor()
    }
    
    fileprivate func configureButtonColor() {
        guard let buttonColor = customizeDelegate?.buttonColor else { return }
        
        let textColor = buttonColor.text
        doneButton.setTitleColor(textColor.active, for: .normal)
        doneButton.setTitleColor(textColor.inactive, for: .disabled)
        
        let backgroundColor = buttonColor.background
        let backgroundActive = UIImage.imageWithColor(color: backgroundColor.active)
        let backgroundInactive = UIImage.imageWithColor(color: backgroundColor.inactive)
        doneButton.setBackgroundImage(backgroundActive, for: .normal)
        doneButton.setBackgroundImage(backgroundInactive, for: .disabled)
        
        recoveryButton.setTitleColor(backgroundColor.active, for: .normal)
        recoveryButton.setTitleColor(backgroundColor.inactive, for: .disabled)
    }
    
}

// MARK: - Keyboard

extension PasswordController {
    
    override var scrollToFrame: CGRect {
        get { return doneButton.frame }
    }
    
    override func keyboardDidHide(notification: Notification) {
        if isKeyboardShow == false { return }
        
        super.keyboardDidHide(notification: notification)
        if isContinue {
            performSegue(withIdentifier: "New Code Segue", sender: nil)
        }
    }
    
}

// MARK: - Callback

extension PasswordController {
    
    func responseCompletedRequest(responce: ResponseAuthentication) {
        activity.stopAnimating()
        recoveryButton.isHidden = false
        doneButton.isEnabled = true
        
        switch responce.stage {
            case .pin: if isKeyboardShow { isContinue = true }
                       else { performSegue(withIdentifier: "New Code Segue", sender: nil) }
            
            case .error: prepareError(exception: responce.data)
            default: break
        }
    }
    
    fileprivate func prepareError(exception: Any?) {
        if let error = exception as? Error {
            failure(error)
            doneButton.isEnabled = false
        }
    }
}

// MARK: Navigation Controller Callback

extension PasswordController: NavigationControllerBackButtonDelegate {
    
    public func viewControllerShouldPopOnBackButton() -> Bool {
        self.navigationController?.popToRootViewController(animated: true)
        return false
    }
}
